package de.szut.springboot_crud_service_demo.controller;

import java.util.List;

import de.szut.springboot_crud_service_demo.dao.PersonDao;
import de.szut.springboot_crud_service_demo.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
	private PersonDao personDao;
	
    // http://localhost:8080/SpringBootCrudService/
    @RequestMapping("/SpringBootCrudService/")
    public String welcome() {
        logger.info("PersonRestController.welcome()");
        return "Welcome to the Spring Boot CRUD Service.";
    }
    
    // http://localhost:8080/SpringBootCrudService/person/{id}
    @GetMapping(value = "/SpringBootCrudService/person/{id}",
            	produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Person> getPersonByUrl(@PathVariable("id") int id) {
        logger.info("PersonRestController.getPersonByUrl()");
        Person personRead = personDao.read(id);
        if (personRead == null) {
        	return ResponseEntity.notFound().build();
        }
        else {
        	return ResponseEntity.ok(personRead);
        }
    }
    
    // http://localhost:8080/SpringBootCrudService/person?id={id}
    @GetMapping(value = "/SpringBootCrudService/person",
            	produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Person> getPersonByParam(@RequestParam("id") int id) {
        logger.info("PersonRestController.getPersonByParam()");
        Person personRead = personDao.read(id);
        if (personRead == null) {
        	return ResponseEntity.notFound().build();
        }
        else {
        	return ResponseEntity.ok(personRead);
        }
    }
    
    // http://localhost:8080/SpringBootCrudService/allpersons
    @GetMapping(value = "/SpringBootCrudService/allpersons",
            	produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<List<Person>> getAllPersons() {
        logger.info("PersonRestController.getAllPersons()");
        List<Person> personList = personDao.read();
        if (personList == null) {
        	return ResponseEntity.notFound().build();
        }
        else if (personList.size() == 0) {
        	return ResponseEntity.notFound().build();
        }
        else {
        	return ResponseEntity.ok(personList);
        }
    }
    
    // http://localhost:8080/SpringBootCrudService/person
    @PostMapping(value = "/SpringBootCrudService/person",
   				consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
   				produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Person> addPerson(@RequestBody Person person) {
        logger.info("PersonRestController.addPerson()");
        Person personCreated = personDao.create(person);
        if (personCreated == null) {
        	return ResponseEntity.notFound().build();
        }
        else {
        	return ResponseEntity.ok(personCreated);
        }
    }
    
    // http://localhost:8080/SpringBootCrudService/person
    @PutMapping(value = "/SpringBootCrudService/person",
				consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            	produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Person> updatePerson(@RequestBody Person person) {
        logger.info("PersonRestController.updatePerson()");
        Person personUpdated = personDao.update(person);
        if (personUpdated == null) {
        	return ResponseEntity.notFound().build();
        }
        else {
        	return ResponseEntity.ok(personUpdated);
        }
    }
 
    // http://localhost:8080/SpringBootCrudService/person/{id}
    @DeleteMapping(value = "/SpringBootCrudService/person/{id}",
            		produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Object> deletePersonByUrl(@PathVariable("id") int id) {
        logger.info("PersonRestController.deletePersonByUrl()");
        personDao.delete(id);
        return ResponseEntity.noContent().build();
    }
    
    // http://localhost:8080/SpringBootCrudService/person?id={id}
    @DeleteMapping(value = "/SpringBootCrudService/person",
            		produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResponseEntity<Object> deletePersonByParam(@RequestParam("id") int id) {
        logger.info("PersonRestController.deletePersonByParam()");
        personDao.delete(id);
        return ResponseEntity.noContent().build();
    }

}
